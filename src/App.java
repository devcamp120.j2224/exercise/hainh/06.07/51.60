import java.util.ArrayList;
import java.util.Collections;

    public class App {
        public static void main(String[] args) throws Exception {
            System.out.println("Hello, World!");
            App.arrayToArrayListLong();
            App.arrayToArrayListDouble();
        }
        // conver từ array sang arraylist xài kiểu dữ liệu Long
        public static void arrayToArrayListLong(){
            Long[] lo = new Long[100];   
            Long resul ;
            for(int i = 0 ; i < lo.length ; i++){
                // ép kiểu 
                lo[i] = (long) (i + 1);
                resul = lo[i];
                ArrayList<Long> arrayList = new ArrayList<>();
                Collections.addAll(arrayList, resul); 
                System.out.println(arrayList);  
            }
        }
        // conver từ array sang arraylist xài kiểu dữ liệu Double
        public static void arrayToArrayListDouble(){
            Double[] lo = new Double[100];   
            Double resul ;
            for(int i = 0 ; i < lo.length ; i++){
                // ép kiểu 
                lo[i] = (double) (i + 1);
                resul = lo[i];
                ArrayList<Double> arrayList = new ArrayList<>();
                Collections.addAll(arrayList, resul); 
                System.out.println(arrayList);  
            }
        }
    }

